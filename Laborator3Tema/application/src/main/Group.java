package main;

import java.util.ArrayList;

public class Group<G extends Client>implements Comparable<Group<G>> {

	private String name;
	private ArrayList<G> members=new ArrayList<>();
	
	public Group(String name) {
		this.name=name;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean addClient(G client) {
		if(members.contains(client)) {
			System.out.println(client.getName() + "is already in this group.");
			return false;
		} else {
			members.add(client);
			System.out.println(client.getName() + " is added in group "+this.getName());
			return true;
		}
	}
	
	public int numberOfClients() {
		return members.size();
	}
	
	@Override
	public int compareTo(Group<G> group) {
		if(this.numberOfClients()>group.numberOfClients())
			return 1;
		else if(this.numberOfClients()<group.numberOfClients())
				return -1;
		return 0;
	}
}
