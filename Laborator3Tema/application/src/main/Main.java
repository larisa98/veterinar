package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import application.Animal;
import application.Personalmedical;
import application.Programare;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.DatabaseUtil;
/**
 * @author Larisa
 *
 */
public  class Main extends Application{
	
	public void start(Stage primaryStage) throws Exception {
		try {
			Parent root1=(Parent) FXMLLoader.load(getClass().getClassLoader().getResource("controllers/Login.fxml"));
			Scene scene1=new Scene(root1,500,320);
			primaryStage.setScene(scene1);
			primaryStage.show();
			
			
			//BorderPane root=(BorderPane)FXMLLoader.load(getClass().getClassLoader().getResource("controllers/MainView.fxml"));
			//Scene scene = new Scene(root,800,800);
			//primaryStage.setScene(scene);
			//primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static Map<Integer,Landmark> landmarks =new HashMap<>();
	
	public static void main(String[] args) throws Exception {
		
		Group<PersonClient> group1=new Group<>("Group1");
		PersonClient person1=new PersonClient("Mihai Avram");
		PersonClient person2=new PersonClient("Vlad Matron");
		PersonClient person3=new PersonClient("Alexandru Ionescu");
		PersonClient person4=new PersonClient("Dorina Florea");
		group1.addClient(person1);
		group1.addClient(person2);
		group1.addClient(person3);
		group1.addClient(person4);
		Group<PersonClient> group2=new Group<>("Group2");
		PersonClient person5=new PersonClient("Claudiu");
		PersonClient person6=new PersonClient("Maria");
		PersonClient person7=new PersonClient("Ana");
		group2.addClient(person5);
		group2.addClient(person6);
		group2.addClient(person7);

		Group<FirmClient> group3=new Group<>("Group3");
		FirmClient firm1=new FirmClient("PetShop");
		FirmClient firm2=new FirmClient("ZooPlanet");
		FirmClient firm3=new FirmClient("ZoologicalGarden");
		group3.addClient(firm1);
		group3.addClient(firm2);
		group3.addClient(firm3);
		Group<FirmClient> group4=new Group<>("Group4");
		FirmClient firm4=new FirmClient("WorldOfAnimals");
		FirmClient firm5=new FirmClient("Animax");
		FirmClient firm6=new FirmClient("ZoologicalGarden");
		group4.addClient(firm4);
		group4.addClient(firm5);
		group4.addClient(firm6);
		
		System.out.println(group1.compareTo(group2));
		System.out.println(group3.compareTo(group4));
		
		List<PersonClient> clients =new ArrayList<>();
		clients.add(person1);
		clients.add(person2);
		clients.add(person3);
		clients.add(person4);
		Collections.sort(clients,(c1,c2) -> c1.getName().compareTo(c2.getName()));
		
		for (PersonClient c : clients) {
        	System.out.println(c.getName());
        }
		
		 UpperConcat uc = (s1,s2) -> s1.toUpperCase()+s2.toUpperCase();
	        String sillyString = doStringStuff(uc, clients.get(0).getName(), clients.get(1).getName());
	        
	        
	        System.out.println(sillyString);

		
	    @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in); 
	    landmarks.put(0, new Landmark(0,"You are looking at a map."));
	    landmarks.put(1, new Landmark(1,"You are at the central train station."));
	    landmarks.put(2, new Landmark(2,"You are at the National Bank."));
	    landmarks.put(3,new Landmark(3,"You are at he Hospital"));
	    landmarks.put(4,new Landmark(4,"You are at the town hall"));
	    landmarks.put(5, new Landmark(5,"You are at the police station"));
	    landmarks.put(6,new Landmark(6," You are in front of the Vet."));
	    
	    landmarks.get(1).addDirection("E", 2);
	    landmarks.get(1).addDirection("W", 3);
	    landmarks.get(1).addDirection("N", 6);
	    landmarks.get(1).addDirection("S", 4);
	    
	    landmarks.get(2).addDirection("N", 1);
	    landmarks.get(2).addDirection("S", 5);
	    
	    landmarks.get(3).addDirection("N", 1);
	    landmarks.get(3).addDirection("S", 4);
	    
	    landmarks.get(4).addDirection("N", 1);
	    landmarks.get(4).addDirection("W", 3);
	    landmarks.get(4).addDirection("S", 6);
	    landmarks.get(4).addDirection("E", 5);
	    
	    landmarks.get(5).addDirection("N", 2);
	    landmarks.get(5).addDirection("W", 4);
	    landmarks.get(5).addDirection("S", 6);
	    
	    landmarks.get(6).addDirection("W", 1);
	    landmarks.get(6).addDirection("N", 4);
	    landmarks.get(6).addDirection("E", 5);
	    
	    Map<String,String> voc=new HashMap<>();
	    voc.put("Q","QUIT");
	    voc.put("N", "NORTH");
	    voc.put("S", "SOUTH");
	    voc.put("E", "EAST");
	    voc.put("W", "WEST");
	    
	    int loc=1;
	    while(true) {
	    	System.out.println(landmarks.get(loc).getDescription());
	    	if(loc==0)
	    		break;
	    	Map<String,Integer> a=landmarks.get(loc).getDirections();
	    	System.out.println("IF YOU WANT TO LOGIN PLEASE ENTER 'Q' (QUIT)!!!!!!");
	    	System.out.println("Available landmarks are: ");
	    	for(String x: a.keySet())
	    		System.out.println(voc.get(x)+", ");
	    	System.out.println();
	    	String dir=scanner.nextLine().toUpperCase();
	    	if(a.containsKey(dir)) 
	    		loc=a.get(dir);
	    	else
	    		System.out.println("You can not go there.");
	    }
	    
	    
		launch(args);
		DatabaseUtil dbUtil=new DatabaseUtil();
		Animal animal=new Animal();
		Personalmedical pers=new Personalmedical();		
		Programare prog=new Programare();
		/**
	 * @param 
	 * create Animal 
	 */
		//DatabaseUtil.createAnimal(animal, 5, "Bobby");
		/**
	 * @param 
	 * read Animal
	 */
		//DatabaseUtil.readAnimal(animal,3);
		/**
	 * @param 
	 *	update Animal, where id=1 to name=Boty
	 */
		//DatabaseUtil.updateAnimal(animal,5,"Boty");
		
		/**
	 * @param
	 * delete Animal, where id=1
	 */
		//DatabaseUtil.deleteAnimal(animal,1);
		
		/**
	 * @param 
	 * create Personalmedical
	 */
		//DatabaseUtil.createPersonalmedical(pers,14,"Dr. Marcel Antol");
		
	    /**
	 * @param 
	 * read Personal Medical
	 */
		//DatabaseUtil.readPersonalmedical(pers, 13);
		/**
	 * @param 
	 * update Personal, where id=1 to name=Dr. Marton Ion
	 */
		//DatabaseUtil.updatePersonalmedical(pers,14,"Dr. Marcel Antol");
		
		/**
	 * @param
	 * delete Personalmedical, where id=1
	 */
		//DatabaseUtil.deletePersonalmedical(pers,1);
		
		/**
	 * @param 
	 * create Programare
	 */
		//DatabaseUtil.createProgramare(prog,21,"08:00","11.02.2019");
		
		/**
	 * @param 
     * read Programare
	 */
		//DatabaseUtil.readProgramare(prog, 23);
		
		/**
	 * @param 
	 * update Porgramare, where id=1 to date=23.02.2019
	 */
		//DatabaseUtil.updateProgramare(prog,23,"23.02.2019","08:20");
	
		/**
	 * @param 
	 * delete Programare, where id=1
	 */
		//DatabaseUtil.deleteProgramare(prog,1);
	
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal);
		dbUtil.savePersonalmedical(pers);
		dbUtil.savePrograms(prog);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.printAllPersonalMedicalFromDB();
		dbUtil.printAllProgramsFromDB();
		dbUtil.closeEntityManager();	
	} 
	
	public static final String doStringStuff(UpperConcat uc, String s1, String s2) {
		return  uc.upperAndConcat(s1, s2);
	}
	
}
interface UpperConcat {
	public String upperAndConcat(String s1, String s2);
}
