package main;

import java.util.HashMap;
import java.util.Map;

public class Landmark {
	
	private final int landmarkId;
	private final String description;
	private final Map<String , Integer> directions;
	
	public Landmark(int landmarkId,String description) {
		this.landmarkId=landmarkId;
		this.description=description;
		directions=new HashMap<String,Integer>();
		directions.put("Q", 0);
	}
	
	public int getLandmarkId() {
		return landmarkId;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Map<String,Integer> getDirections(){
		return directions;
	}
	
	public void addDirection(String direction,int landmark) {
		directions.put(direction, landmark);
	}
	
	
	

}
