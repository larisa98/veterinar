package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import util.DatabaseUtil;
import application.Animal;
import application.Programare;

public class MainController implements Initializable{

	@FXML
	private Label idStatus;
	@FXML
	private TextField idUsername;
	@FXML
	private TextField idPassword;
	@FXML
	private ListView<String> listView;
	@FXML 
	private Button idIstoric;
	@FXML
	private TextField idNume;
	@FXML
	private TextField idTip;
	@FXML
	private TextField idVarsta;
	@FXML
	private TextField idGreutate;
	@FXML
	private TextField idSpecie;
	
	public void Login(ActionEvent event) throws Exception {
		if(idUsername.getText().equals("user")&&idPassword.getText().equals("0000")) {
			idStatus.setText("Login Success");
			Stage primaryStage=new Stage();
			
			BorderPane root=(BorderPane)FXMLLoader.load(getClass().getClassLoader().getResource("controllers/MainView.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		else {
			idStatus.setText("Login Failed");
		}
	}
	
	
	public void populateMainListView() throws Exception {
		DatabaseUtil db=new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList= (List<Animal>) db.animalList();
		ObservableList<Animal> animalList=getAnimalObservableList(animalDBList);
		//listView.setItems(animalNamesList);
		//List<Programare> prog= (List<Programare>) db.progList();
		List<String> oreProgAndAnimType=(List<String>) db.hourAndTypeList();
		ObservableList<String> oreProgAndType=getOraProgramareAndAnimalType(oreProgAndAnimType);
		
		listView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				for(int i=0;i<animalList.size();++i) {
					if(listView.getSelectionModel().getSelectedIndex()==i) {
						idNume.setText(animalList.get(i).getName());
						idTip.setText(animalList.get(i).getType());
						idVarsta.setText(Integer.toString(animalList.get(i).getAge()));
						idGreutate.setText(Double.toString(animalList.get(i).getWeight()));
						idSpecie.setText(animalList.get(i).getSpecies());
					}
				}
				
			}
			
		});
		
		listView.setItems(oreProgAndType);
		listView.refresh();
		db.closeEntityManager();
		
		
	}
	public ObservableList<String> getOraProgramareAndAnimalType(List<String> list){
		ObservableList<String> progAndType=FXCollections.observableArrayList();
		for(String s:list) {
			progAndType.add(s);
		}
		return progAndType;
	}
	
	public ObservableList<String> getOraProgramare(List<Programare> programari){
		ObservableList<String> prog=FXCollections.observableArrayList();
		for(Programare p: programari) {
			prog.add(p.getOraProgramare());
		}
		return prog;
	}
	public ObservableList<String> getAnimalName(List<Animal> animals){
		ObservableList<String> names=FXCollections.observableArrayList();
		for(Animal a:animals) {
			names.add(a.getName());
		}
		return names;
	}

	public ObservableList<Animal> getAnimalObservableList(List<Animal> animals){
		ObservableList<Animal> animalss=FXCollections.observableArrayList();
		for(Animal a:animals) {
			animalss.add(a);
		}
		return animalss;
	}

	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			populateMainListView();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
