package util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import application.Animal;
import application.Personalmedical;
import application.Programare;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	public void setUp() throws Exception{
		entityManagerFactory=Persistence.createEntityManagerFactory("application");
		entityManager=entityManagerFactory.createEntityManager();
	}
	
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	public void savePersonalmedical(Personalmedical persMedical) {
		entityManager.persist(persMedical);
	}
	public void savePrograms(Programare programare) {
		entityManager.persist(programare);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager(){
		entityManager.close();
	}
	
	public void printAllAnimalsFromDB() {
		@SuppressWarnings("unchecked")
		List<Animal> results1=entityManager.createNativeQuery("Select * from application.Animal",Animal.class)
				.getResultList();
		for(Animal animal:results1) {
			System.out.println("Animal :"+animal.getName()+" has ID: "+animal.getIdAnimal()+" type: "+animal.getType()+" age: "+animal.getAge()+" weight: "+animal.getWeight()+" species: "+animal.getSpecies());
		}
	}
	public void printAllPersonalMedicalFromDB() {
		@SuppressWarnings("unchecked")
		List<Personalmedical> results2=entityManager.createNativeQuery("Select * from application.Personalmedical",Personalmedical.class)
				.getResultList();
		for(Personalmedical personal:results2) {
			System.out.println("Personal Medical :"+personal.getName()+" has ID: "+personal.getIdPersonalMedical());
		}
	}
	public void printAllProgramsFromDB() {
		@SuppressWarnings("unchecked")
		List<Programare> results3=entityManager.createNativeQuery("Select * from application.Programare",Programare.class)
				.getResultList();
		for(Programare programare:results3) {
			System.out.println("Programarea :"+programare.getIdProgramare()+" has date: "+programare.getDataProgramare()+" at time: "+programare.getOraProgramare());
		}
	}
	
	public static void createAnimal(Animal animal,int i, String string,int age,String species,String type,double weight) {
		animal=new Animal();
		animal.setIdAnimal(i);
		animal.setName(string);
		animal.setAge(age);
		animal.setSpecies(species);
		animal.setType(type);
		animal.setWeight(weight);
	}
	public static void createPersonalmedical(Personalmedical pers, int i, String string) {
		pers=new Personalmedical();
		pers.setIdPersonalMedical(i);
		pers.setName(string);
		
	}
	public static void createProgramare(Programare prog, int i, String string,String oraProgramare) {
		prog=new Programare();
		prog.setIdProgramare(i);
		prog.setDataProgramare(string);
		prog.setOraProgramare(oraProgramare);
	}

	public static void readAnimal(Animal animal,int i) {
		@SuppressWarnings("unchecked")
		List<Animal> results1=entityManager.createNativeQuery("Select * from application.Animal",Animal.class)
				.getResultList();
		for(Animal animal1:results1)
			if(animal1.getIdAnimal()==i) {
				System.out.println("ID Animal : " + animal1.getIdAnimal());
		        System.out.println("Name :" + animal1.getName() );
		        System.out.println("Age: "+animal1.getAge());
		        System.out.println("Species: "+animal1.getSpecies());
		        System.out.println("Type: "+animal1.getType());
		        System.out.println("Weight: "+animal1.getWeight());
			}
	}
	public static void readPersonalmedical(Personalmedical pers,int i) {
		@SuppressWarnings("unchecked")
		List<Personalmedical> results2=entityManager.createNativeQuery("Select * from application.Personalmedical",Personalmedical.class)
				.getResultList();
		for(Personalmedical personal:results2)
			if(personal.getIdPersonalMedical()==i) {
				System.out.println("ID Personal Medical : "+pers.getIdPersonalMedical());
				System.out.println("Name : "+ pers.getName());
			}
	}
	public static void readProgramare(Programare prog,int i) {
		@SuppressWarnings("unchecked")
		List<Programare> results3=entityManager.createNativeQuery("Select * from application.Programare",Programare.class)
				.getResultList();
		for(Programare programare:results3)
			if(programare.getIdProgramare()==i) {
				System.out.println("ID Programare : "+ prog.getIdProgramare());
				System.out.println("Data Programare : "+prog.getDataProgramare());
				System.out.println("Ora Programare : "+prog.getOraProgramare());
			}
	}
	
	public static void updateAnimal(Animal animal, int i,String name,int age,String species,String type,double weight) {
		@SuppressWarnings("unchecked")
		List<Animal> results1=entityManager.createNativeQuery("Select * from application.Animal",Animal.class)
				.getResultList();
		for(Animal animal1:results1)
			if(animal1.getIdAnimal()==i) {
				animal.setName(name);
				animal.setAge(age);
				animal.setSpecies(species);
				animal.setType(type);
				animal.setWeight(weight);
			}
	}
	public static void updatePersonalmedical(Personalmedical pers, int i,String name) {
		@SuppressWarnings("unchecked")
		List<Personalmedical> results2=entityManager.createNativeQuery("Select * from application.Personalmedical",Personalmedical.class)
				.getResultList();
		for(Personalmedical personal:results2)
			if(personal.getIdPersonalMedical()==i) {
				pers.setName(name);
			}
	}
	public static void updateProgramare(Programare prog, int i, String string,String oraProg) {
		@SuppressWarnings("unchecked")
		List<Programare> results3=entityManager.createNativeQuery("Select * from application.Programare",Programare.class)
				.getResultList();
		for(Programare programare:results3)
			if(programare.getIdProgramare()==i) {
				prog.setDataProgramare(string);
				prog.setOraProgramare(oraProg);
			}
	}

	public static void deleteAnimal(Animal animal, int i) {
		@SuppressWarnings("unchecked")
		List<Animal> results1=entityManager.createNativeQuery("Select * from application.Animal",Animal.class)
				.getResultList();
		for(Animal animal1:results1)
			if(animal1.getIdAnimal()==i)
				results1.remove(animal1);
	}
	public static void deletePersonalmedical(Personalmedical pers, int i) {
		@SuppressWarnings("unchecked")
		List<Personalmedical> results2=entityManager.createNativeQuery("Select * from application.Personalmedical",Personalmedical.class)
				.getResultList();
		for(Personalmedical personal:results2)
			if(personal.getIdPersonalMedical()==i)
				results2.remove(personal);
	}
	public static void deleteProgramare(Programare prog, int i) {
		@SuppressWarnings("unchecked")
		List<Programare> results3=entityManager.createNativeQuery("Select * from application.Programare",Programare.class)
				.getResultList();
		for(Programare programare:results3)
			if(programare.getIdProgramare()==i)
				results3.remove(programare);
	}

	public List<Animal> animalList() {
		List<Animal> animalList=(List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
	public List<Programare> progList() {
		List<Programare> prog=(List<Programare>)entityManager.createQuery("SELECT p FROM Programare p",Programare.class).getResultList();
		return prog;
	}
	public List<String> hourAndTypeList(){
		List<Programare> prog=(List<Programare>)entityManager.createQuery("SELECT p FROM Programare p",Programare.class).getResultList();
		List<Animal> animal=(List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		List<String> hourProgAndAnimalType=new ArrayList<String>();
		String aux1,aux2;
		for(int i=0;i<prog.size();i++) {
			aux1=prog.get(i).getOraProgramare();
			aux2=animal.get(i).getType();
			
			hourProgAndAnimalType.add(i, aux1+" " +aux2);
		}
		
		return hourProgAndAnimalType;
	}

}
