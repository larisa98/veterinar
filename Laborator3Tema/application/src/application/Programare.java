package application;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	private String dataProgramare;

	private String oraProgramare;

	public Programare() {
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public String getDataProgramare() {
		return this.dataProgramare;
	}

	public void setDataProgramare(String dataProgramare) {
		this.dataProgramare = dataProgramare;
	}

	public String getOraProgramare() {
		return this.oraProgramare;
	}

	public void setOraProgramare(String oraProgramare) {
		this.oraProgramare = oraProgramare;
	}

}